import type { Type } from './Type'

type Product = {
  id?: number
  name: string
  price: number
  image: string
  type: Type
}
function getImageUrl(product: Product) {
  return `http://localhost:3000/img/products/${product.image}.png`
}
export { type Product, getImageUrl }
