import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const snackbar = ref(false)
  const text = ref('')
  const showSnackbar = (message: string) => {
    text.value = message
    snackbar.value = true
  }

  return { showSnackbar, snackbar, text }
})
