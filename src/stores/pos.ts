import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import productService from '@/services/product'


export const usePosStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()
  const msgStore = useMessageStore()
  const products1 = ref<Product[]>([])  
  const products2 = ref<Product[]>([])  
  const products3 = ref<Product[]>([])  

  async function getProducts() {
    try {
      loadingStore.doLoad()
      const res1 = await productService.getProductsByType(1)
      products1.value = res1.data
      const res2 = await productService.getProductsByType(2)
      products2.value = res2.data
      const res3 = await productService.getProductsByType(3)
      products3.value = res3.data
      loadingStore.finish()
    } catch (e: any) {
      msgStore.showSnackbar(e.message)
      loadingStore.finish()
    }
  }

  return { products1, products2, products3, getProducts }
})
