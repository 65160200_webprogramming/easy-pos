import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import menu from '../components/MainMenu.vue'
import header from '../components/MainAppBar.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: HomeView,
        menu: menu,
        header: header
      },
      meta: {
        layout: 'MainLayout',
        requiredAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/AboutView.vue'),
        menu: menu,
        header: header
      },
      meta: {
        layout: 'MainLayout',
        requiredAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/UsersView.vue'),
        menu: menu,
        header: header
      },
      meta: {
        layout: 'MainLayout',
        requiredAuth: true
      }
    },
    {
      path: '/products',
      name: 'products',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/ProductsView.vue'),
        menu: menu,
        header: header
      },
      meta: {
        layout: 'MainLayout',
        requiredAuth: true
      }
    },
    {
      path: '/pos',
      name: 'pos',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/pos/PosView.vue'),
        menu: menu,
        header: header
      },
      meta: {
        layout: 'MainLayout',
        requiredAuth: true
      }
    },
    {
      path: '/temperature',
      name: 'temperature',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/TemperatureView.vue'),
        menu: menu,
        header: header
      },
      meta: {
        layout: 'MainLayout',
        requiredAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requiredAuth: false
      }
    }
  ]
})

function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}

router.beforeEach((to, from) => {
  if (to.meta.requiredAuth && !isLogin()) {
    router.replace('/login')
  }
})
export default router
